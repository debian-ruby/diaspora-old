Source: diaspora
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Build-Depends: debhelper (>= 7.0.50~), gem2deb (>= 0.7.5~)
Standards-Version: 3.9.5
Vcs-Git: git://anonscm.debian.org/pkg-ruby-extras/diaspora.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-ruby-extras/diaspora.git;a=summary
Homepage: http://diasporafoundation.org
XS-Ruby-Versions: all

Package: diaspora
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${shlibs:Depends}, ${misc:Depends}, ruby | ruby-interpreter,
 ruby-rails-3.2,
 unicorn,
 ruby-acts-as-api,
 ruby-json,
 ruby-devise,
 ruby-galetahub-simple-captcha,
 ruby-sidekiq,
 ruby-sinatra,
 ruby-configurate,
 ruby-rack-cors,
 ruby-mysql2,
 ruby-pg,
 ruby-activerecord-import,
 ruby-foreigner,
 ruby-carrierwave,
 ruby-fog,
 ruby-mini-magick,
 ruby-remotipart,
 ruby-http-accept-language,
 ruby-i18n-inflector-rails,
 ruby-rails-i18n,
 ruby-markerb,
 ruby-messagebus-api,
 ruby-nokogiri,
 ruby-rails-autolink,
 ruby-redcarpet,
 ruby-roxml,
 ruby-oembed,
 ruby-opengraph-parser,
 ruby-strong-parameters,
 ruby-omniauth,
 ruby-omniauth-facebook,
 ruby-omniauth-tumblr,
 ruby-omniauth-twitter,
 ruby-twitter,
 ruby-omniauth-wordpress,
 ruby-acts-as-taggable-on,
 ruby-addressable,
 ruby-faraday,
 ruby-faraday-middleware,
 ruby-typhoeus,
 ruby-client-side-validations,
 ruby-gon,
 ruby-haml,
 ruby-mobile-fu,
 ruby-will-paginate,
 ruby-zip-zip,
 ruby-entypo-rails,
 ruby-bootstrap-sass,
 ruby-compass-rails,
 ruby-sass-rails,
 ruby-uglifier,
 ruby-backbone-on-rails,
 ruby-handlebars-assets,
 ruby-jquery-rails,
 nodejs 
Description: distributed social networking service
 Diaspora (currently styled diaspora* and formerly styled DIASPORA*) is a free
 personal web server that implements a distributed social networking service.
 Installations of the software form nodes (termed "pods") which make up the
 distributed Diaspora social network.
 .
 Diaspora is intended to address privacy concerns related to centralized
 social networks by allowing users set up their own server (or "pod") to
 host content; pods can then interact to share status updates, photographs,
 and other social data. It allows its users to host their data with a
 traditional web host, a cloud-based host, an ISP, or a friend. The framework,
 which is being built on Ruby on Rails, is free software and can be
 experimented with by external developers.
 .
 Learn more about diaspora at http://diasporafoundation.org
